package changescene;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SecondWindow extends Application {

	@Override
	public void start(Stage stage) {
		VBox layout2 = new VBox(20);    
		stage.setTitle("JavaFX Second Window");
		Label lbl2 = new Label("This is a Second Window VBox Layout.");
		Button btnSubmit2= new Button("Go to scene 1");
		layout2.getChildren().addAll(lbl2, btnSubmit2);
		
		Scene scene2 = new Scene(layout2, 500, 500);
		stage.setScene(scene2);
		stage.show();
		
		btnSubmit2.setOnAction(e-> {
			// Instantiate the class that creates a new scene
			// Call method in newly instantiated class, passing primaryStage to it
			FirstWindow firstWindow = new FirstWindow();
			firstWindow.start(stage);
			});
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
