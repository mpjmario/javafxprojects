package changescene;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class FirstWindow extends Application {

	@Override
	public void start(Stage stage) {
		VBox layout1 = new VBox(20); 
		stage.setTitle("JavaFX First Window");
		Label lbl1 = new Label("This is a First window VBox layout");
		Button btnSubmit1= new Button("Go to scene 2");
		layout1.getChildren().addAll(lbl1, btnSubmit1);
		
		Scene scene1 = new Scene(layout1, 500, 500);
		stage.setScene(scene1);
		stage.show();
		
		btnSubmit1.setOnAction(e-> {
			// Instantiate the class that creates a new scene
			// Call method in newly instantiated class, passing primaryStage to it
			SecondWindow secondWindow = new SecondWindow();
			secondWindow.start(stage);
			});
		
	}

	public static void main(String[] args) {
		launch(args);
	}
}
